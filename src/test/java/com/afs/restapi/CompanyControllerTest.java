package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.Random.class)
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JPACompanyRepository jpaCompanyRepository;

    @Autowired
    private JPAEmployeeRepository jpaEmployeeRepository;

    @BeforeEach
    void setUp() {
        jpaCompanyRepository.deleteAll();
        jpaEmployeeRepository.deleteAll();
    }

    @Test
    void should_update_company_name() throws Exception {
        Long id = generateNextId();
        Company previousCompany = new Company(id, "abc");
        jpaCompanyRepository.save(previousCompany);

        Company companyUpdateRequest = new Company(id, "xyz");
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(companyUpdateRequest);
        mockMvc.perform(put("/companies/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<Company> optionalCompany = jpaCompanyRepository.findById(id);
        assertTrue(optionalCompany.isPresent());
        Company updatedCompany = optionalCompany.get();
        Assertions.assertEquals(previousCompany.getId(), updatedCompany.getId());
        Assertions.assertEquals(companyUpdateRequest.getName(), updatedCompany.getName());
    }

    @Test
    void should_delete_company_name() throws Exception {
        Long id = generateNextId();
        Company company = new Company(id, "abc");
        jpaCompanyRepository.save(company);

        Employee employee = getEmployee(company);
        jpaEmployeeRepository.save(employee);
        EmployeeControllerTest.generateNextId();

        mockMvc.perform(delete("/companies/{id}", id))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(jpaCompanyRepository.findById(id).isEmpty());

        assertFalse(jpaCompanyRepository.findById(company.getId()).isPresent());
        assertFalse(jpaEmployeeRepository.findById(employee.getId()).isPresent());
    }

    @Test
    void should_create_company() throws Exception {
        CompanyRequest companyRequest = getCompanyRequest1();
        generateNextId();

        ObjectMapper objectMapper = new ObjectMapper();
        String companyRequestString = objectMapper.writeValueAsString(companyRequest);
        mockMvc.perform(post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyRequestString))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(companyRequest.getName()));
    }

    @Test
    void should_find_companies() throws Exception {
        Company company = getCompany1();
        jpaCompanyRepository.save(company);

        Long id = generateNextId();
        mockMvc.perform(get("/companies"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(id))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company.getName()));
    }

    @Test
    void should_find_companies_by_page() throws Exception {
        Company company1 = getCompany1();
        Company company2 = getCompany2();
        Company company3 = getCompany3();
        jpaCompanyRepository.save(company1);
        jpaCompanyRepository.save(company2);
        jpaCompanyRepository.save(company3);

        Long id1 = generateNextId();
        Long id2 = generateNextId();
        Long id3 = generateNextId();
        mockMvc.perform(get("/companies")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(id1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(id2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(company2.getName()))
        ;
    }

    @Test
    void should_find_company_by_id() throws Exception {
        Company company = getCompany1();
        jpaCompanyRepository.save(company);
        Employee employee = getEmployee(company);
        jpaEmployeeRepository.save(employee);
        EmployeeControllerTest.generateNextId();

        Long id = generateNextId();
        mockMvc.perform(get("/companies/{id}", id))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(id))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(1));
    }

    @Test
    void should_find_employees_by_companies() throws Exception {
        Company company = getCompany1();
        jpaCompanyRepository.save(company);
        Employee employee = getEmployee(company);
        jpaEmployeeRepository.save(employee);
        EmployeeControllerTest.generateNextId();

        Long id = generateNextId();
        mockMvc.perform(get("/companies/{companyId}/employees", id))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(employee.getSalary()));
    }

    private static Employee getEmployee(Company company) {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        employee.setCompanyId(company.getId());
        return employee;
    }


    private static Company getCompany1() {
        Company company = new Company();
        company.setName("ABC");
        return company;
    }

    private static Company getCompany2() {
        Company company = new Company();
        company.setName("DEF");
        return company;
    }

    private static Company getCompany3() {
        Company company = new Company();
        company.setName("XYZ");
        return company;
    }

    private static Long nextId = 0L;

    private static Long generateNextId() {
        nextId++;
        return nextId;
    }

    private CompanyRequest getCompanyRequest1() {
        CompanyRequest companyRequest = new CompanyRequest();
        companyRequest.setName("ABC");

        return companyRequest;
    }

    private CompanyRequest getCompanyRequest2() {
        CompanyRequest companyRequest = new CompanyRequest();
        companyRequest.setName("DEF");

        return companyRequest;
    }

    private CompanyRequest getCompanyRequest3() {
        CompanyRequest companyRequest = new CompanyRequest();
        companyRequest.setName("XYZ");

        return companyRequest;
    }
}