package com.afs.restapi.controller;

import com.afs.restapi.service.TodoService;
import com.afs.restapi.service.dto.TodoRequest;
import com.afs.restapi.service.dto.TodoResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/todos")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todosService) {
        this.todoService = todosService;
    }

    @GetMapping
    public List<TodoResponse> getAllTodo() {
        return todoService.findAll();
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable("id") Long id) {
        return todoService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse addTodo(@RequestBody TodoRequest todoRequest) {
        return todoService.addTodo(todoRequest);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public TodoResponse updateTodo(@PathVariable("id") Long id, @RequestBody TodoRequest todoRequest) {
        return todoService.updateTodo(id, todoRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable("id") Long id) {
        todoService.deleteTodo(id);
    }

}
