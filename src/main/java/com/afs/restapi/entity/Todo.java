package com.afs.restapi.entity;

import javax.persistence.*;

@Entity
@Table(name = "todo")
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String text;
    private String description;
    private Boolean done;

    public Todo() {
    }

    public Todo(Long id, String text, String description, Boolean done) {
        this.id = id;
        this.text = text;
        this.description = description;
        this.done = done;
    }

    public Todo(String text, String description, Boolean done) {
        this.text = text;
        this.description = description;
        this.done = done;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public void update(Todo todo) {
        this.text = todo.getText() == null ? this.text : todo.getText();
        this.description = todo.getDescription() == null ? this.description : todo.getDescription();
        this.done = todo.getDone() == null ? this.done : todo.getDone();
    }
}
