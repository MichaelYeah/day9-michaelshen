package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    public final JPAEmployeeRepository jpaEmployeeRepository;

    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        List<Employee> all = jpaEmployeeRepository.findAll();
        return all.stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public EmployeeResponse findById(Long id) {
        Employee employee = findEmployeeById(id);
        return EmployeeMapper.toResponse(employee);
    }

    private Employee findEmployeeById(Long id) {
        return jpaEmployeeRepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, EmployeeRequest employeeRequest) {
        if (employeeRequest == null) return;
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        Employee toBeUpdatedEmployee = findEmployeeById(id);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        jpaEmployeeRepository.save(toBeUpdatedEmployee);
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        List<Employee> allByGender = jpaEmployeeRepository.findAllByGender(gender);
        return allByGender.stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);

        Employee savedEmployee = jpaEmployeeRepository.save(employee);
        return EmployeeMapper.toResponse(savedEmployee);
    }

    public List<EmployeeResponse> findByPage(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        List<Employee> employees = jpaEmployeeRepository.findAll(pageable).toList();
        return employees.stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
