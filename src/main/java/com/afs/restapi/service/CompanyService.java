package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;
    private final JPAEmployeeRepository jpaEmployeeRepository;

    public CompanyService(JPACompanyRepository inMemoryCompanyRepository, JPAEmployeeRepository inMemoryEmployeeRepository) {
        this.jpaCompanyRepository = inMemoryCompanyRepository;
        this.jpaEmployeeRepository = inMemoryEmployeeRepository;
    }

    public List<CompanyResponse> findAll() {
        List<Company> all = jpaCompanyRepository.findAll();
        return all.stream()
                .map(CompanyMapper::toResponse)
                .collect(Collectors.toList());
    }

    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        PageRequest pageable = PageRequest.of(page - 1, size);
        List<Company> companies = jpaCompanyRepository.findAll(pageable).toList();
        return companies.stream()
                .map(CompanyMapper::toResponse)
                .collect(Collectors.toList());
    }

    public CompanyResponse findById(Long id) {
        Company company = getCompanyById(id);
//        List<Employee> employees = jpaEmployeeRepository.findByCompanyId(company.getId());
//        company.setEmployees(employees);
        return CompanyMapper.toResponse(company);
    }

    private Company getCompanyById(Long id) {
        return jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }


    public void update(Long id, CompanyRequest companyRequest) {
        if (companyRequest == null) return;
        Company company = CompanyMapper.toEntity(companyRequest);
        Company companyToUpdate = getCompanyById(id);
        companyToUpdate.setName(company.getName());
        jpaCompanyRepository.save(companyToUpdate);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = CompanyMapper.toEntity(companyRequest);
        Company save = jpaCompanyRepository.save(company);
        return CompanyMapper.toResponse(save);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return jpaEmployeeRepository.findByCompanyId(id);
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
