package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Todo;
import com.afs.restapi.service.dto.TodoRequest;
import com.afs.restapi.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static Todo toEntity(TodoRequest todosRequest) {
        Todo todos = new Todo();
        BeanUtils.copyProperties(todosRequest, todos);

        return todos;
    }

    public static TodoResponse toResponse(Todo savedTodos) {
        TodoResponse todosResponse = new TodoResponse();
        BeanUtils.copyProperties(savedTodos, todosResponse);

        return todosResponse;
    }
}
