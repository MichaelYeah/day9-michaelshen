package com.afs.restapi.intergration;

import com.afs.restapi.entity.Todo;
import com.afs.restapi.repository.JPATodoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class TodoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JPATodoRepository jpaTodoRepository;

    @BeforeEach
    void setUp() {
        jpaTodoRepository.deleteAll();
    }

    @Test
    void should_return_all_todos_when_getAllTodos_given_todos() throws Exception {
        Todo todo = getTodo1();
        Todo saved = jpaTodoRepository.save(todo);

        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(saved.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(saved.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value(saved.getDescription()));
    }

    @Test
    void should_return_todo_when_getTodoById_given_id() throws Exception {
        Todo todo = getTodo1();
        Todo saved = jpaTodoRepository.save(todo);

        mockMvc.perform(get("/todos/{id}", saved.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(saved.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(saved.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(saved.getDescription()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(saved.getDone()));
    }

    @Test
    void should_throw_exception_when_getTodoNById_given_wrong_id() throws Exception {
        Todo todo = getTodo1();
        Todo saved = jpaTodoRepository.save(todo);

        MvcResult mvcResult = mockMvc.perform(get("/todos/{id}", 666)).andReturn();
        assertEquals(404, mvcResult.getResponse().getStatus());
    }

    @Test
    void should_update_todo() throws Exception {
        Todo previousTodo = getTodo1();
        Todo saved1 = jpaTodoRepository.save(previousTodo);

        Todo todoToUpdateTo = getTodo2();
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(todoToUpdateTo);
        mockMvc.perform(put("/todos/{id}", saved1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<Todo> optionalTodo = jpaTodoRepository.findById(saved1.getId());
        assertTrue(optionalTodo.isPresent());
        Todo updatedTodo = optionalTodo.get();
        Assertions.assertEquals(previousTodo.getId(), updatedTodo.getId());
        Assertions.assertEquals(todoToUpdateTo.getText(), updatedTodo.getText());
        Assertions.assertEquals(todoToUpdateTo.getDescription(), updatedTodo.getDescription());
        Assertions.assertEquals(todoToUpdateTo.getDone(), updatedTodo.getDone());
    }

    @Test
    void should_delete_todo() throws Exception {
        Todo todo1 = getTodo1();
        jpaTodoRepository.save(todo1);

        Todo todo2 = getTodo2();
        jpaTodoRepository.save(todo2);

        mockMvc.perform(delete("/todos/{id}", todo2.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(jpaTodoRepository.findById(todo2.getId()).isEmpty());

        assertTrue(jpaTodoRepository.findById(todo1.getId()).isPresent());
    }

    private Todo getTodo1() {
        Todo todo = new Todo();
        todo.setText("text1");
        todo.setDescription("1");
        todo.setDone(false);
        return todo;
    }

    private Todo getTodo2() {
        Todo todo = new Todo();
        todo.setText("text2");
        todo.setDescription("2");
        todo.setDone(true);
        return todo;
    }

}