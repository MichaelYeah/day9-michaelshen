package com.afs.restapi.service;

import com.afs.restapi.entity.Todo;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.exception.TodoNotFoundException;
import com.afs.restapi.repository.JPATodoRepository;
import com.afs.restapi.service.dto.TodoRequest;
import com.afs.restapi.service.dto.TodoResponse;
import com.afs.restapi.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoService {
    public final JPATodoRepository jpaTodoRepository;

    public TodoService(JPATodoRepository jpaTodosRepository) {
        this.jpaTodoRepository = jpaTodosRepository;
    }

    public List<TodoResponse> findAll() {
        List<Todo> all = jpaTodoRepository.findAll();
        return all.stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    private Todo findTodoById(Long id) {
        return jpaTodoRepository.findById(id)
                .orElseThrow(TodoNotFoundException::new);
    }

    public TodoResponse addTodo(TodoRequest todoRequest) {
        Todo todo = TodoMapper.toEntity(todoRequest);
        return TodoMapper.toResponse(jpaTodoRepository.save(todo));
    }

    public TodoResponse updateTodo(Long id, TodoRequest todoRequest) {
        Todo todoFindById = findTodoById(id);
        todoFindById.update(TodoMapper.toEntity(todoRequest));
        return TodoMapper.toResponse(jpaTodoRepository.save(todoFindById));
    }

    public void deleteTodo(Long id) {
        Todo todo = new Todo();
        todo.setId(id);
        jpaTodoRepository.delete(todo);
    }

    public TodoResponse getById(Long id) {
        Todo todoById = findTodoById(id);
        return TodoMapper.toResponse(todoById);
    }
}
