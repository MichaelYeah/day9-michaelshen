create table if not exists todo (

    id   bigint auto_increment primary key,

    text varchar(255) null,

    description varchar(255) null,

    done BOOLEAN

);